// Invoke strict mode.
"use strict";

// PROMISES.ALL.

// 1a. 'promise1': resolve a promise.
const promise1 = Promise.resolve("G'day World!");
// 1b. 'promise2': store a variable.
const promise2 = 10;
// 1c. 'promise3': create a new promise with setTimeout().
const promise3 = new Promise((resolve, reject) => setTimeout(resolve, 2000, "Goodbye Dudes."));
// 1d. 'promise4': promise with fetch data from http://jsonplaceholder.typicode.com.
const promise4 = fetch('http://jsonplaceholder.typicode.com/users').then(res => res.json());
// 2. 'Promise.all()': resolve all defined promises together.
Promise.all(
    [
        promise1, 
        promise2, 
        promise3, 
        promise4
    ]
)
    .then(values => console.log(values))
    .catch(err => console.log(err));
