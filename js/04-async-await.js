// Invoke strict mode.
"use strict";

// ASYNC AWAIT.

// 1. 'films': an array of film titles.
const films = [
    {
        title: 'The Black Cat', 
        synopsis: 'American honeymooners in Hungary become trapped in the home of a Satan-worshipping priest when the bride is taken there for medical help following a road accident.'
    }, 
    {
        title: 'The Mummy', 
        synopsis: 'An Egyptian mummy searches Cairo for the girl he thinks is his long-lost princess.'
    }
];
// 2. 'getFilms': a function to access the films information.
function getFilms() {
    setTimeout(() => {
        let output = '';
        output += `<ul>`;
        films.forEach((film, index) => {
            output += ` <li>
                        <h2>${film.title}</h2>
                        <p>${film.synopsis}</p>
                        </li>`;
        });
        output += `</ul>`;
        document.body.innerHTML = output;
    }, 1000);
}
// 3. 'createFilm': a function to add a film title and information to the 'films' array. It uses 'callback' to delay the calling of 'getFilms'.
function createFilm(film) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            films.push(film);
            // Error checking.
            const error = false;
            if(!error) {
                resolve();
            }
            else {
                reject('Hey Bro, something went wrong Bro.');
            }
        }, 2000);
    });
}
// 4. 'init()': an async function that awaits an event before calling getFilms().
async function init() {
    await createFilm(
        {
            title: 'The Locked Door', 
            synopsis: 'On her first anniversary Ann Reagan finds that her sister-in-law is involved with a shady character from her own past, and determines to intervene.'
        }
    );
    getFilms();
}
init();
// 5. 'fetchUsers()': an async function that awaits for fetch of data from http://jsonplaceholder.typicode.com.
async function fetchUsers() {
    const res = await fetch('http://jsonplaceholder.typicode.com/users');
    const data = await res.json();
    console.log(data)
}
fetchUsers();