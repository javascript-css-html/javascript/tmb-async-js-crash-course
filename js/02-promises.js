// Invoke strict mode.
"use strict";

// PROMISES.

// 1. 'films': an array of film titles.
const films = [
    {
        title: 'The Black Cat', 
        synopsis: 'American honeymooners in Hungary become trapped in the home of a Satan-worshipping priest when the bride is taken there for medical help following a road accident.'
    }, 
    {
        title: 'The Mummy', 
        synopsis: 'An Egyptian mummy searches Cairo for the girl he thinks is his long-lost princess.'
    }
];
// 2. 'getFilms': a function to access the films information.
function getFilms() {
    setTimeout(() => {
        let output = '';
        output += `<ul>`;
        films.forEach((film, index) => {
            output += ` <li>
                        <h2>${film.title}</h2>
                        <p>${film.synopsis}</p>
                        </li>`;
        });
        output += `</ul>`;
        document.body.innerHTML = output;
    }, 1000);
}
// 3. 'createFilm': a function to add a film title and information to the 'films' array. It uses 'callback' to delay the calling of 'getFilms'.
function createFilm(film) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            films.push(film);
            // Error checking.
            const error = true;
            if(!error) {
                resolve();
            }
            else {
                reject('Hey Bro, something went wrong Bro.');
            }
        }, 4000);
    });
}
// 4. 'createFilm': create a new film entry by calling this function.
createFilm(
    {
        title: 'Mystery of the Wax Museum', 
        synopsis: 'The disappearance of people and corpses leads a reporter to a wax museum and a sinister sculptor.'
    }
)
    .then(getFilms) // Becomes part of the createFilm() function.
    .catch(err => console.log(err));
    // getFilms();