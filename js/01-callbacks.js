// Invoke strict mode.
"use strict";

// CALLBACKS.

// 1. 'films': an array of film titles.
const films = [
    {
        title: 'The Black Cat', 
        synopsis: 'American honeymooners in Hungary become trapped in the home of a Satan-worshipping priest when the bride is taken there for medical help following a road accident.'
    }, 
    {
        title: 'The Mummy', 
        synopsis: 'An Egyptian mummy searches Cairo for the girl he thinks is his long-lost princess.'
    }
];
// 2. 'getFilms': a function to access the films information.
function getFilms() {
    setTimeout(() => {
        let output = '';
        output += `<ul>`;
        films.forEach((film, index) => {
            output += ` <li>
                        <h2>${film.title}</h2>
                        <p>${film.synopsis}</p>
                        </li>`;
        });
        output += `</ul>`;
        document.body.innerHTML = output;
    }, 1000);
}
// 3. 'createFilm': a function to add a film title and information to the 'films' array.
function createFilm(film, callback) {
    setTimeout(() => {
        films.push(film);
        callback();
    }, 2000);
}
// 4a. 'getFilms': call function.
// getFilms();

// 4b. 'createFilm': create a new film entry by calling this function.
createFilm(
    {
        title: "The Mummy's Hand", 
        synopsis: 'Archaeologists defile the tomb of mummified Kharis, who was buried alive for falling in love with an Egyptian princess.'
    },
    getFilms
);


